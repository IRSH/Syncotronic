# Copyright © 2018 Marcel Radzio
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#, fuzzy
msgid   ""
msgstr  "Project-Id-Version: \n"
        "Report-Msgid-Bugs-To: EMAIL\n"
        "POT-Creation-Date: 2018-06-19 12:11+0200\n"
        "PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
        "Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
        "Language-Team: LANGUAGE <LL@li.org>\n"
        "Language: \n"
        "MIME-Version: 1.0\n"
        "Content-Type: text/plain; charset=CHARSET\n"
        "Content-Transfer-Encoding: 8bit\n"

#: pkg/commands/help/help.go:87
msgid   "Show this help message"
msgstr  ""

#: pkg/commands/help/help.go:36
msgid   "Help"
msgstr  ""

#: pkg/commands/intellink/intellink.go:39
msgid   "Listens to intel links and returns information about them"
msgstr  ""

#: pkg/commands/setlanguage/setlanguage.go:56
msgid   "Set the channel language using ISO 639-1 codes (for example de\\_DE)"
msgstr  ""

#: pkg/commands/sync/startsync.go:33
msgid   "Starts a Sync between this Room and the Destination Room"
msgstr  ""

#: pkg/commands/sync/stopsync.go:32
msgid   "Removes a Sync between this Room and the Destination Room"
msgstr  ""

#: pkg/commands/whereami/whereami.go:34
msgid   "Returns the platform type and channelID of the current room"
msgstr  ""