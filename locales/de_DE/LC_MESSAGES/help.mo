��          \      �       �      �   9   �   9     ;   B  C   ~     �  8   �  S       f  H   l  8   �  F   �  [   5     �  9   �                                       Help Listens to intel links and returns information about them Removes a Sync between this Room and the Destination Room Returns the platform type and channelID of the current room Set the channel language using ISO 639-1 codes (for example de\_DE) Show this help message Starts a Sync between this Room and the Destination Room Project-Id-Version: 
Report-Msgid-Bugs-To: EMAIL
POT-Creation-Date: 2018-06-19 12:11+0200
PO-Revision-Date: 2018-07-12 19:51+0200
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.9
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n != 1);
Language: de_DE
 Hilfe Hört auf intel links und antwortet mit den dazugehörigen Informationen Entfernt den Sync zwischen diesem Raum und den Ziel Raum Antwortet mit dem Plattform Typen und der ChannelID von aktuellen Raum Die Sprache für diesen Channel mithilfe von ISO 639-1 Codes setzen. (Zum Beispiel: de\_DE) Zeigt diese Hilfe Nachricht Startet einen Sync zwischen diesen Raum und dem Ziel Raum 