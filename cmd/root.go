// Copyright © 2018 Marcel Radzio
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/config"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/db/sqlite"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/distributor/sync"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/metrics"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/util"
	"github.com/leonelquinteros/gotext"
	"github.com/lestrrat-go/file-rotatelogs"
	"github.com/pkg/errors"
	"github.com/rifflock/lfshook"
	"github.com/shibukawa/configdir"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"go/build"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"

	// Register Bot types
	_ "git.nordgedanken.de/IRSH/Syncotronic/pkg/bots/discord"
	_ "git.nordgedanken.de/IRSH/Syncotronic/pkg/bots/slack"
	_ "git.nordgedanken.de/IRSH/Syncotronic/pkg/bots/telegram"

	// Register Command Types
	_ "git.nordgedanken.de/IRSH/Syncotronic/pkg/commands/help"
	_ "git.nordgedanken.de/IRSH/Syncotronic/pkg/commands/intellink"
	_ "git.nordgedanken.de/IRSH/Syncotronic/pkg/commands/nick"
	_ "git.nordgedanken.de/IRSH/Syncotronic/pkg/commands/setlanguage"
	_ "git.nordgedanken.de/IRSH/Syncotronic/pkg/commands/sync"
	_ "git.nordgedanken.de/IRSH/Syncotronic/pkg/commands/whereami"
)

// register flags
func init() {
	util.Debug = rootCmd.Flags().Bool("debug", false, "Activate Debug logs")
}

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use: "Syncotronic",
	PreRunE: func(cmd *cobra.Command, args []string) error {
		// Init Logs and folders
		// configdir.New gets the Config Paths that are available on this system
		configDirs := configdir.New("IRSH", "Syncotronic")
		// path holds the global configdir of the system with the log folder appended
		path := filepath.ToSlash(configDirs.QueryFolders(configdir.Global)[0].Path + "/log/")
		logFilePath := filepath.ToSlash(path + "main.log")

		// This checks if the directory is missing and if that's the case to generate the directory
		if _, StatErr := os.Stat(path); os.IsNotExist(StatErr) {
			MkdirErr := os.MkdirAll(path, 0755)
			if MkdirErr != nil {
				return errors.Wrap(MkdirErr, "PreRunE MkdirAll")
			}
		}

		writer, err := rotatelogs.New(
			logFilePath+".%Y%m%d%H%M",
			rotatelogs.WithMaxAge(time.Duration(86400)*time.Second),
			rotatelogs.WithRotationTime(time.Duration(604800)*time.Second),
		)
		if err != nil {
			return errors.Wrap(err, "PreRunE rotatelogs")
		}

		log.AddHook(lfshook.NewHook(
			lfshook.WriterMap{
				log.InfoLevel:  writer,
				log.ErrorLevel: writer,
			},
			&log.TextFormatter{},
		))

		if *util.Debug {
			log.SetLevel(log.DebugLevel)
		}

		// dbImpl.Init() generates the needed tables if needed before the app starts
		util.DB = &sqlite.SQLite{}
		err = util.DB.Init()
		if err != nil {
			return errors.Wrap(err, "PreRunE DB Init")
		}

		log.Infoln("DB Set Up")

		log.Infoln("Setting up Localization")
		gopath := os.Getenv("GOPATH")
		if gopath == "" {
			gopath = build.Default.GOPATH
		}
		util.LocalesRoot = filepath.Join(gopath, "src", "git.nordgedanken.de", "IRSH", "Syncotronic", "locales")

		gotext.Configure(util.LocalesRoot, "en", "default")
		log.Infoln("Localization set up")
		return nil
	},

	Short: "",
	Long:  ``,
	RunE: func(cmd *cobra.Command, args []string) error {
		log.Infoln("Loading Config next")

		if util.Config == nil {
			util.Config = &config.Config{}
		}

		err := util.Config.Load()
		if err != nil {
			return err
		}

		log.Infoln("Config loaded")
		log.Infoln("Starting Bots...")

		for i, v := range util.Config.BotsMap {
			util.Botwg.Add(1)
			log.Infof("[%s] Starting...", i)
			go v.StartBot()
		}

		go sync.LoadSyncsFromMem()

		go metrics.Start()

		util.Botwg.Add(1)
		go func() {
			sc := make(chan os.Signal, 1)
			signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
			<-sc
			for _, v := range util.Config.BotsMap {
				err := v.StopBot()
				if err != nil {
					log.Errorln(err)
					os.Exit(1)
				}
			}
			log.Infoln("All bots are stopped!")

			util.Botwg.Done()
		}()

		util.Botwg.Wait()
		return nil
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Printf("%+v\n", err)
		os.Exit(1)
	}
}
