package util

import (
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/config"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/db"
	"sync"
)

// DB holds the main pointer to the Database
var DB db.DB

var Helps map[string]string

var Botwg sync.WaitGroup

var LocalesRoot string

var Config *config.Config

var Debug *bool
