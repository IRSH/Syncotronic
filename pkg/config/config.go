package config

import (
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/bots"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/commands"
	"github.com/BurntSushi/toml"
	"github.com/pkg/errors"
	"github.com/shibukawa/configdir"
	"github.com/sirupsen/logrus"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

type Config struct {
	botTypes        map[string]bots.Bot
	commandTypes    map[string]commands.Command
	rawCommandTypes map[string]*RawCommand
	BotsMap         map[string]bots.Bot
	Bots            *Bots
}

type Bots map[string]*Bot

type Bot struct {
	AccessToken string `toml:"access_token"`
	Commands    []*Commands
}

type Commands struct {
	Type   string
	Active bool
}

type RawCommand struct {
	command commands.Command
	regex   *regexp.Regexp
}

func (c *Config) RegisterBotType(btype string, bot bots.Bot) {
	if c.botTypes == nil {
		c.botTypes = make(map[string]bots.Bot)
	}
	c.botTypes[btype] = bot
}

func (c *Config) RegisterCommandType(ctype string, command commands.Command) {
	if c.commandTypes == nil {
		c.commandTypes = make(map[string]commands.Command)
	}
	c.commandTypes[ctype] = command
}

func (c *Config) RegisterRawCommandType(ctype string, command commands.Command, regex *regexp.Regexp) {
	if c.rawCommandTypes == nil {
		c.rawCommandTypes = make(map[string]*RawCommand)
	}

	rawCommand := &RawCommand{
		command: command,
		regex:   regex,
	}
	c.rawCommandTypes[ctype] = rawCommand
}

func (c *Config) Load() error {
	// configdir.New gets the Config Paths that are available on this system
	configDirs := configdir.New("IRSH", "Syncotronic")
	// path holds the global configdir of the system with the log folder appended
	path := filepath.ToSlash(configDirs.QueryFolders(configdir.Global)[0].Path)
	configFilePath := filepath.ToSlash(path + "/config.toml")

	// This checks if the config is missing
	if _, StatErr := os.Stat(configFilePath); os.IsNotExist(StatErr) {
		return errors.New("Config Load file not found at: " + configFilePath)
	}

	c.Bots = &Bots{}
	if c.BotsMap == nil {
		c.BotsMap = make(map[string]bots.Bot)
	}

	_, err := toml.DecodeFile(configFilePath, c.Bots)
	if err != nil {
		return err
	}

	for i, bot := range *c.Bots {
		botName := strings.ToLower(i)
		c.BotsMap[botName] = c.botTypes[botName]
		for _, command := range bot.Commands {
			if command.Active {
				// Register Help
				if c.commandTypes[command.Type] != nil {
					logrus.Debugln("Registering ", command.Type, "...")
					//Register Help
					c.commandTypes[command.Type].AddHelp()

					// Get the bot specific command regexp
					cregexp, err := c.botTypes[botName].GetCommandRegexp(command.Type)
					if err != nil {
						continue
					}

					// Register Command to bot
					c.botTypes[botName].AddCommand(c.commandTypes[command.Type], cregexp)
				} else if c.rawCommandTypes[command.Type] != nil {
					logrus.Debugln("Registering ", command.Type, "...")
					//Register Help
					c.rawCommandTypes[command.Type].command.AddHelp()

					// Register Command to bot
					c.botTypes[botName].AddCommand(c.rawCommandTypes[command.Type].command, c.rawCommandTypes[command.Type].regex)
				} else {
					logrus.Warnf("Command of type %s not found \n", command.Type)
				}
			}
		}
	}

	return nil
}
