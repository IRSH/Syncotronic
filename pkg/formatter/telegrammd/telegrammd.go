package telegrammd

import (
	"bytes"
	"fmt"
)

type TelegramMD struct{}

func (t *TelegramMD) GetTitle(title string) string {
	return fmt.Sprintf("*%s*", title)
}

func (t *TelegramMD) GetList(list []string) string {
	var listB bytes.Buffer
	for _, v := range list {
		listB.WriteString("- ")
		listB.WriteString(v)
		listB.WriteString("\n")
	}

	return listB.String()
}

func (t *TelegramMD) GetLink(linktext, link string) string {
	return link
}

func (t *TelegramMD) GetCode(code string) string {
	return fmt.Sprintf("```\n%s\n```", code)
}

func (t *TelegramMD) GetCodeSimple(code string) string {
	return fmt.Sprintf("`%s`", code)
}

func (t *TelegramMD) GetBold(text string) string {
	return fmt.Sprintf("*%s*", text)
}

func (t *TelegramMD) GetItalic(text string) string {
	return fmt.Sprintf("_%s_", text)
}

func (t *TelegramMD) GetBoldItalic(text string) string {
	return fmt.Sprintf("_*%s*_", text)
}
