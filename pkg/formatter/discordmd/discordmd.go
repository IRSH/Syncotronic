package discordmd

import (
	"bytes"
	"fmt"
)

type DiscordMD struct{}

func (d *DiscordMD) GetTitle(title string) string {
	return title + "\n"
}

func (d *DiscordMD) GetList(list []string) string {
	var listB bytes.Buffer
	for _, v := range list {
		listB.WriteString("- ")
		listB.WriteString(v)
		listB.WriteString("\n")
	}

	return listB.String()
}

func (d *DiscordMD) GetLink(linktext, link string) string {
	return link
}

func (d *DiscordMD) GetCode(code string) string {
	return fmt.Sprintf("```\n%s\n```", code)
}

func (d *DiscordMD) GetCodeSimple(code string) string {
	return fmt.Sprintf("`%s`", code)
}

func (d *DiscordMD) GetBold(text string) string {
	return fmt.Sprintf("**%s**", text)
}

func (d *DiscordMD) GetItalic(text string) string {
	return fmt.Sprintf("*%s*", text)
}

func (d *DiscordMD) GetBoldItalic(text string) string {
	return fmt.Sprintf("***%s***", text)
}
