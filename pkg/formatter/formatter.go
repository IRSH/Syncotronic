package formatter

type Formatter interface {
	GetTitle(title string) string
	GetList(list []string) string
	GetLink(linktext, link string) string
	GetCode(code string) string
	GetCodeSimple(code string) string
	GetBold(text string) string
	GetItalic(text string) string
	GetBoldItalic(text string) string
}
