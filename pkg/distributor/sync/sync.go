package sync

import (
	"fmt"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/db"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/util"
	"github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
)

func LoadSyncsFromMem() error {
	syncs, err := util.DB.GetSyncPools()
	if err != nil {
		return err
	}

	for _, v := range syncs {
		AddSyncListeners(v)
	}
	return nil
}

func RegisterSync(originS, destinationS, originC, destinationC string) error {
	err := util.DB.AddTable(`CREATE TABLE IF NOT EXISTS sync (id integer PRIMARY KEY, targets text NOT NULL)`)
	if err != nil {
		return err
	}

	id, syncs, err := util.DB.GetSyncPoolByChannel(destinationC, destinationS, originC, originS)

	var one bool
	var two bool
	for _, v := range syncs {
		if v.Network == originS && v.ChannelID == originC {
			one = true
		}
		if v.Network == destinationS && v.ChannelID == destinationC {
			two = true
		}
	}
	if !one {
		uuidO, err := uuid.NewV4()
		if err != nil {
			logrus.Errorln("Something went wrong: %s", err)
		}
		origin := &db.Sync{
			ChannelID: originC,
			Network:   originS,
			UUID:      uuidO.String(),
		}
		syncs = append(syncs, origin)
	}
	if !two {
		uidO, err := uuid.NewV4()
		if err != nil {
			logrus.Errorln("Something went wrong: %s", err)
		}
		destination := &db.Sync{
			ChannelID: destinationC,
			Network:   destinationS,
			UUID:      uidO.String(),
		}
		syncs = append(syncs, destination)
	}

	_, err = util.DB.AddSyncPool(syncs, id)
	if err != nil {
		return err
	}

	err = util.Config.BotsMap[originS].SendText("", "", "", originC, fmt.Sprintf("Sync with %s on %s created", destinationC, destinationS))
	if err != nil {
		return err
	}

	err = util.Config.BotsMap[destinationS].SendText("", "", "", destinationC, fmt.Sprintf("Sync with %s on %s created", originC, originS))
	if err != nil {
		return err
	}

	AddSyncListeners(syncs)
	return nil
}

func RemoveSync(originC, originS, destinationC, destinationS string) error {
	id, syncs, err := util.DB.GetSyncPoolByChannel(destinationC, destinationS, originC, originS)
	err = util.DB.RemoveSyncByID(id, syncs, destinationS, destinationC)
	if err != nil {
		return err
	}

	util.Config.BotsMap[destinationS].RemoveRoomListener(fmt.Sprintf("%s_%s_%s_%s", destinationS, destinationC, originS, originC))

	err = util.Config.BotsMap[originS].SendText("", "", "", originC, fmt.Sprintf("Sync with %s on %s stopped", destinationC, destinationS))
	if err != nil {
		return err
	}

	return util.Config.BotsMap[destinationS].SendText("", "", "", destinationC, fmt.Sprintf("Sync with %s on %s stopped", originC, originS))
}

func AddSyncListeners(syncs []*db.Sync) {
	for i := 0; i < len(syncs); i++ {
		originS := syncs[0].Network
		originC := syncs[0].ChannelID
		originUUID := syncs[0].UUID

		syncs = append(syncs[:0], syncs[0+1:]...)

		for _, v := range syncs {
			// Add Listener
			target := v
			err := util.Config.BotsMap[originS].AddRoomListener(
				fmt.Sprintf("%s%s", originUUID, target.UUID),
				originC,
				target.Network,
				target.ChannelID,
			)
			if err != nil {
				logrus.Errorln(err)
			}
		}

		newS := &db.Sync{
			Network:   originS,
			ChannelID: originC,
			UUID:      originUUID,
		}
		syncs = append(syncs, newS)
	}
}
