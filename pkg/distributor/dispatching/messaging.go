package dispatching

import (
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/util"
	"github.com/kyokomi/emoji"
	"github.com/sirupsen/logrus"
	"io"
)

func SendText(senderName, avatarS, avatarURL, text, service, channel string) error {
	logrus.Debugln("Sending text to ", service)
	emojiText := emoji.Sprint(text)
	return util.Config.BotsMap[service].SendText(senderName, avatarS, avatarURL, channel, emojiText)
}

func SendImage(senderName, avatarS, avatarURL, text, service, channel, filename, fileURL string, file io.Reader) error {
	logrus.Debugln("Sending Image to ", service)
	emojiText := emoji.Sprint(text)
	return util.Config.BotsMap[service].SendImage(senderName, avatarS, avatarURL, emojiText, channel, filename, fileURL, file)
}
