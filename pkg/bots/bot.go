package bots

import (
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/commands"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/formatter"
	"io"
	"regexp"
)

// Define a Object holding Sync Target Data
type Sync struct {
	ID      string
	Network string
	Channel string
}

type Bot interface {
	StartBot() error
	StopBot() error

	GetCommandRegexp(command string) (*regexp.Regexp, error)
	AddCommand(command commands.Command, regexp *regexp.Regexp) error

	AddRoomListener(id, channelID, networkT, channelT string) error
	RemoveRoomListener(id string)

	GetFormatter() formatter.Formatter

	SendText(senderName, avatarS, avatarURL, channel, text string) error
	SendImage(senderName, avatarS, avatarURL, text, channel, filename, fileURL string, file io.Reader) error

	GetSync() map[string][]*Sync
}
