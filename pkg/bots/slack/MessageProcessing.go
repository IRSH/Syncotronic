package slack

import (
	"fmt"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/distributor/dispatching"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/util"
	"github.com/mvdan/xurls"
	"github.com/nlopes/slack"
	"github.com/pkg/errors"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

func (s *Slack) processHOBotImages(avatarURL, name, text, network, channel string) error {
	possibleImageURL := xurls.Strict.FindString(text)
	if strings.Contains(possibleImageURL, "googleusercontent.com") {
		imageURL, urlParsingError := url.Parse(possibleImageURL)
		if urlParsingError == nil {
			pathSplit := strings.Split(imageURL.EscapedPath(), "/")
			filename := pathSplit[len(pathSplit)-1]

			resp, err := http.Get(imageURL.String())
			if err != nil {
				return errors.New("Failed getting the Image from Slack!")
			}

			avatarS, err := s.GetAvatarData(avatarURL)
			if err != nil {
				return err
			}
			err = dispatching.SendImage(
				name,
				avatarS,
				avatarURL,
				"", // No Text as Hangouts Sync does that for us already
				network,
				channel,
				filename,
				imageURL.String(),
				resp.Body,
			)
			if err != nil {
				return errors.New(fmt.Sprintf("Failed sending the Image to: %s - %s", network, channel))
			}

			return resp.Body.Close()
		}
	}
	return nil
}

func (s *Slack) processAttachments(attachments []slack.Attachment, avatarURL, name, text, network, channel string) error {
	for _, a := range attachments {
		if a.ImageURL != "" {
			parsedImageURL, err := url.Parse(a.ImageURL)
			if err != nil {
				return errors.New("Failed parsing the Image Direct URL!")
			}

			pathSplit := strings.Split(parsedImageURL.EscapedPath(), "/")
			filename := strconv.Itoa(a.ID) + pathSplit[len(pathSplit)-1]

			resp, err := http.Get(parsedImageURL.String())
			if err != nil {
				return errors.New("Failed getting the Image from Slack!")
			}

			avatarS, err := s.GetAvatarData(avatarURL)
			if err != nil {
				return err
			}

			clearedText := slackMagicTagRe.ReplaceAllString(text, "")

			mentionClearedText := clearedText
			submatches := slackMagicMentionIDRe.FindStringSubmatch(clearedText)
			if len(submatches) > 0 {
				matchID := submatches[1]
				matchUser, err := s.api.GetUserInfo(matchID)
				if err != nil {
					return err
				}
				mentionClearedText = slackMagicMentionRe.ReplaceAllString(clearedText, fmt.Sprintf("@%s", matchUser.Name))
			}

			err = dispatching.SendImage(
				name,
				avatarS,
				avatarURL,
				mentionClearedText,
				network,
				channel,
				filename,
				parsedImageURL.String(),
				resp.Body,
			)
			if err != nil {
				return errors.New(fmt.Sprintf("Failed sending the Image to: %s - %s", network, channel))
			}

			return resp.Body.Close()
		}
	}
	return nil
}

func (s *Slack) processFiles(files []slack.File, avatarURL, name, text, network, channel string) error {
	for _, a := range files {
		if a.URLPrivateDownload != "" && strings.Contains(a.Mimetype, "image") {
			parsedImageURL, err := url.Parse(a.URLPrivateDownload)
			if err != nil {
				return errors.New("Failed parsing the Image Direct URL!")
			}

			pathSplit := strings.Split(parsedImageURL.EscapedPath(), "/")
			filename := a.ID + pathSplit[len(pathSplit)-1]

			client := &http.Client{}
			req, err := http.NewRequest("GET", parsedImageURL.String(), nil)
			if err != nil {
				return errors.New("Failed creating request for image from Slack!")
			}
			slackConfig := *util.Config.Bots
			req.Header.Set("Authorization", "Bearer "+slackConfig["Slack"].AccessToken)
			resp, err := client.Do(req)
			if err != nil {
				return errors.New("Failed getting the Image from Slack!")
			}

			avatarS, err := s.GetAvatarData(avatarURL)
			if err != nil {
				return err
			}

			clearedText := slackMagicTagRe.ReplaceAllString(text, "")

			mentionClearedText := clearedText
			submatches := slackMagicMentionIDRe.FindStringSubmatch(clearedText)
			if len(submatches) > 0 {
				matchID := submatches[1]
				matchUser, err := s.api.GetUserInfo(matchID)
				if err != nil {
					return err
				}
				mentionClearedText = slackMagicMentionRe.ReplaceAllString(clearedText, fmt.Sprintf("@%s", matchUser.Name))
			}

			err = dispatching.SendImage(
				name,
				avatarS,
				avatarURL,
				mentionClearedText,
				network,
				channel,
				filename,
				parsedImageURL.String(),
				resp.Body,
			)
			if err != nil {
				return errors.New(fmt.Sprintf("Failed sending the Image to: %s - %s", network, channel))
			}

			return resp.Body.Close()
		}
	}
	return nil
}

func (s *Slack) processNormalText(avatarURL, name, text, network, channel string) error {
	avatarS, err := s.GetAvatarData(avatarURL)
	if err != nil {
		return err
	}

	clearedText := slackMagicTagRe.ReplaceAllString(text, "")

	mentionClearedText := clearedText
	submatches := slackMagicMentionIDRe.FindStringSubmatch(clearedText)
	if len(submatches) > 0 {
		matchID := submatches[1]
		matchUser, err := s.api.GetUserInfo(matchID)
		if err != nil {
			return err
		}
		mentionClearedText = slackMagicMentionRe.ReplaceAllString(clearedText, fmt.Sprintf("@%s", matchUser.Name))
	}

	err = dispatching.SendText(
		name,
		avatarS,
		avatarURL,
		mentionClearedText,
		network,
		channel,
	)
	if err != nil {
		return errors.New(fmt.Sprintf("Failed to send text messagee to: %s - %s", network, channel))
	}
	return nil
}

func (s *Slack) processChangedText(avatarURL, name, text, network, channel string) error {
	avatarS, err := s.GetAvatarData(avatarURL)
	if err != nil {
		return err
	}

	clearedText := slackMagicTagRe.ReplaceAllString(fmt.Sprintf("%s (Edited)", text), "")

	mentionClearedText := clearedText
	submatches := slackMagicMentionIDRe.FindStringSubmatch(clearedText)
	if len(submatches) > 0 {
		matchID := submatches[1]
		matchUser, err := s.api.GetUserInfo(matchID)
		if err != nil {
			return err
		}
		mentionClearedText = slackMagicMentionRe.ReplaceAllString(clearedText, fmt.Sprintf("@%s", matchUser.Name))
	}

	err = dispatching.SendText(
		name,
		avatarS,
		avatarURL,
		mentionClearedText,
		network,
		channel,
	)
	if err != nil {
		return errors.New(fmt.Sprintf("Failed to send text messagee to: %s - %s", network, channel))
	}
	return nil
}
