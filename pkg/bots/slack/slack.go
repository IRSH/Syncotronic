package slack

import (
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/bots"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/commands"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/config"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/formatter"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/formatter/discordmd"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/metrics"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/util"
	"github.com/nlopes/slack"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"io"
	"io/ioutil"
	"net/http"
	"regexp"
	"strings"
)

func init() {
	if util.Config == nil {
		util.Config = &config.Config{}
	}
	util.Config.RegisterBotType("slack", &Slack{})
}

type Slack struct {
	api      *slack.Client
	rtm      *slack.RTM
	commands map[*regexp.Regexp]commands.Command
	syncs    map[string][]*bots.Sync
}

func (s *Slack) GetSync() map[string][]*bots.Sync {
	return s.syncs
}

var slackMagicTagRe = regexp.MustCompile(`(?m)<ho://.*\|.*>`)
var slackMagicMentionIDRe = regexp.MustCompile(`(?m)(?:<@)([a-zA-Z0-9]+)`)
var slackMagicMentionRe = regexp.MustCompile(`(?m)<@.*\|.*>|<@.*>`)

func (s *Slack) StartBot() error {
	for i, v := range *util.Config.Bots {
		if strings.ToLower(i) == "slack" {
			// FIXME not working as intended
			s.api = slack.New(v.AccessToken, slack.OptionDebug(*util.Debug))
		}
	}

	resp, err := s.api.AuthTest()
	if err != nil {
		logrus.Errorln("[SLACK] ", err)
	}
	botUser, err := s.api.GetUserInfo(resp.UserID)
	if err != nil {
		logrus.Errorln("[SLACK] ", err)
	}
	var botBotID = botUser.Profile.BotID

	s.rtm = s.api.NewRTM()
	go s.rtm.ManageConnection()

	for msg := range s.rtm.IncomingEvents {
		switch ev := msg.Data.(type) {
		case *slack.HelloEvent:
			// Ignore hello

		case *slack.ConnectedEvent:
			logrus.Debugf("[SLACK] Infos: %+v", ev.Info)
			logrus.Infoln("[SLACK] Connected!")

		case *slack.DisconnectedEvent:
			logrus.Infoln("[SLACK] Disconnected!")

		case *slack.MessageEvent:
			if botBotID == ev.BotID {
				continue
			}
			if ev.User == s.rtm.GetInfo().User.ID {
				continue
			} else if ev.SubMessage != nil && ev.SubMessage.User == s.rtm.GetInfo().User.ID {
				continue
			}

			switch ev.SubType {
			default:
				metrics.SlackMessages.Inc() // Count Message
				s.processMessage(ev)
			}

		case *slack.LatencyReport:
			metrics.SlackLatency.Set(ev.Value.Seconds())
			logrus.Infof("[SLACK] Current latency: %v", ev.Value.Seconds())

		case *slack.RTMError:
			logrus.Errorln("[SLACK] ", ev.Error())

		case *slack.InvalidAuthEvent:
			logrus.Errorln("[SLACK] Invalid credentials")
			return errors.New("Invalid credentials")

		default:

			// Ignore other events..
			// fmt.Printf("Unexpected: %v\n", msg.Data)
		}
	}
	return nil
}

func (s *Slack) processMessage(ev *slack.MessageEvent) {
	for i, v := range s.commands {
		if i.MatchString(ev.Text) {
			err := v.Work("slack", ev.Username, "", ev.Channel, ev.Text, s.GetFormatter())
			if err != nil {
				logrus.Errorln(err)
			}
			continue
		}
	}

	v, ok := s.syncs[ev.Channel]
	if ok {
		for _, vt := range v {
			var userID string
			if ev.User != "" {
				userID = ev.User
			} else if ev.SubMessage != nil && ev.SubMessage.User != "" {
				userID = ev.SubMessage.User
			}

			var text string
			if ev.Text != "" {
				text = ev.Text
			} else if ev.SubMessage != nil && ev.SubMessage.Text != "" {
				text = ev.SubMessage.Text
			}

			user, err := s.api.GetUserInfo(userID)
			var name string
			var avatarURL string
			username := ev.Username
			if err != nil {
				logrus.Errorln("[SLACK] GetUserInfo: ", err)
				// Don't early exit as hangoutsbot messages would not be synced as they are bots!
				//continue
				name = username
				if ev.Icons != nil {
					avatarURL = ev.Icons.IconURL
				} else if ev.SubMessage != nil && ev.SubMessage.Icons != nil {
					avatarURL = ev.SubMessage.Icons.IconURL
				}
			} else {
				avatarURL = user.Profile.ImageOriginal
				realname := user.RealName
				if realname != "" {
					name = realname
				} else {
					name = username
				}
			}

			err = s.processHOBotImages(avatarURL, name, text, vt.Network, vt.Channel)
			if err != nil {
				logrus.Errorln("[SLACK] ", err)
				continue
			}

			if ev.Attachments != nil && len(ev.Attachments) != 0 {
				err = s.processAttachments(ev.Attachments, avatarURL, name, text, vt.Network, vt.Channel)
				if err != nil {
					logrus.Errorln("[SLACK] ", err)
					continue
				}
			} else if ev.Files != nil && len(ev.Files) != 0 {
				err = s.processFiles(ev.Files, avatarURL, name, text, vt.Network, vt.Channel)
				if err != nil {
					logrus.Errorln("[SLACK] ", err)
					continue
				}
			} else {
				if ev.SubType == "message_changed" {
					logrus.Infoln("[SLACK] Message Changed event")

					err = s.processChangedText(avatarURL, name, text, vt.Network, vt.Channel)
					if err != nil {
						logrus.Errorln("[SLACK] ", err)
						continue
					}
				} else {
					err = s.processNormalText(avatarURL, name, text, vt.Network, vt.Channel)
					if err != nil {
						logrus.Errorln("[SLACK] ", err)
						continue
					}
				}
			}
		}
	}
}

func (s *Slack) GetAvatarData(avatarURL string) (string, error) {
	if avatarURL != "" {
		resp, err := http.Get(avatarURL)
		if err != nil {
			return "", errors.Wrap(err, "Failed getting the Profile Image from Slack!")
		}

		defer resp.Body.Close()

		var avatarS string
		if resp.StatusCode == http.StatusOK {
			bodyBytes, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				return "", err
			}
			avatarS = string(bodyBytes)
		}

		return avatarS, nil
	}
	return "", nil
}

func (s *Slack) StopBot() error {
	logrus.Infoln("Stopping Slack...")
	err := s.rtm.Disconnect()
	util.Botwg.Done()
	return err
}

func (s *Slack) AddCommand(command commands.Command, regexpL *regexp.Regexp) error {
	if s.commands == nil {
		s.commands = make(map[*regexp.Regexp]commands.Command)
	}
	s.commands[regexpL] = command
	return nil
}

func (s *Slack) GetCommandRegexp(command string) (*regexp.Regexp, error) {
	return regexp.Compile("^!" + command)
}

func (s *Slack) SendText(senderName, avatarS, avatarURL, channel, text string) error {
	msg := slack.PostMessageParameters{
		Markdown: true,
	}

	if senderName != "" {
		msg.Username = senderName
	}

	if avatarURL != "" {
		msg.IconURL = avatarURL
	}

	_, _, err := s.api.PostMessage(channel, slack.MsgOptionText(text, false), slack.MsgOptionPostMessageParameters(msg))
	return err
}

func (s *Slack) SendImage(senderName, avatarS, avatarURL, text, channel, filename, fileURL string, file io.Reader) error {
	fileParams := slack.FileUploadParameters{
		Title:    text,
		Filename: filename,
		Reader:   file,
		Channels: []string{channel},
	}

	_, err := s.api.UploadFile(fileParams)
	return err
}

func (s *Slack) AddRoomListener(id, channelID, networkT, channelT string) error {
	if s.syncs == nil {
		s.syncs = make(map[string][]*bots.Sync)
	}

	//Ignore if sync already exists
	for _, v := range s.syncs {
		for _, target := range v {
			if target.ID == id {
				return errors.New("[SLACK] already existing sync")
			}
		}
	}

	sync := &bots.Sync{
		ID:      id,
		Network: networkT,
		Channel: channelT,
	}
	s.syncs[channelID] = append(s.syncs[channelID], sync)
	return nil
}

func (s *Slack) RemoveRoomListener(id string) {
	for i, v := range s.syncs {
		for targetI, target := range v {
			if target.ID == id {
				v[targetI] = v[len(v)-1]
				v[len(v)-1] = nil
				s.syncs[i] = v[:len(v)-1]
			}
		}
	}
}

func (s *Slack) GetFormatter() formatter.Formatter {
	// TODO use correct Formatter!
	return &discordmd.DiscordMD{}
}
