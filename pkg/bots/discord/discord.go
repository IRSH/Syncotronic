package discord

import (
	"bytes"
	"database/sql"
	"encoding/base64"
	"fmt"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/bots"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/commands"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/config"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/distributor/dispatching"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/formatter"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/formatter/discordmd"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/metrics"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/util"
	"github.com/bwmarrin/discordgo"
	"github.com/leonelquinteros/gotext"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"image"
	_ "image/gif"
	"image/jpeg"
	_ "image/jpeg"
	_ "image/png"
	"io"
	"net/http"
	"regexp"
	"strings"
)

func init() {
	if util.Config == nil {
		util.Config = &config.Config{}
	}
	util.Config.RegisterBotType("discord", &Discord{})
}

var mentionsIDRe = regexp.MustCompile(`(?m)(?:@)([a-zA-z0-9]+)`)
var mentionsRe = regexp.MustCompile(`(?m)@[a-zA-z0-9]+`)

type Discord struct {
	commands      map[*regexp.Regexp]commands.Command
	session       *discordgo.Session
	syncListeners map[string]func()
	syncs         map[string][]*bots.Sync
}

func (d *Discord) GetSync() map[string][]*bots.Sync {
	return d.syncs
}

func (d *Discord) StartBot() error {
	var err error
	for i, v := range *util.Config.Bots {
		if strings.ToLower(i) == "discord" {
			// FIXME not working as intended
			d.session, err = discordgo.New("Bot " + v.AccessToken)
			if err != nil {
				return err
			}
		}
	}
	if d.session != nil {
		//d.session.Debug = *util.Debug
		d.session.AddHandler(func(s *discordgo.Session, m *discordgo.MessageCreate) {
			// Ignore all messages created by the bot itself
			// This isn't required in this specific example but it's a good practice.
			if m.Author.ID == s.State.User.ID {
				return
			}

			if m.Author.Bot {
				hooks, err := GetHooksFromDB()
				if err != nil {
					logrus.Errorln("[DISCORD] Getting Hooks: ", err)
				}
				for _, v := range hooks {
					if m.Author.Username == v.Name {
						return
					}
				}
			}

			metrics.DiscordMessages.Inc() // New Message

			for i, v := range d.commands {
				if i.MatchString(m.Content) {
					err := v.Work("discord", m.Author.Username, m.GuildID, m.ChannelID, m.Content, d.GetFormatter())
					if err != nil {
						logrus.Errorln(err)
					}
				}
			}
		})

		return d.session.Open()
	}

	return nil
}

func (d *Discord) StopBot() error {
	logrus.Infoln("Stopping Discord...")
	err := d.session.Close()
	if err != nil {
		return err
	}
	util.Botwg.Done()
	return nil
}

func (d *Discord) AddCommand(command commands.Command, regexpL *regexp.Regexp) error {
	if d.commands == nil {
		d.commands = make(map[*regexp.Regexp]commands.Command)
	}
	d.commands[regexpL] = command
	return nil
}

func (d *Discord) GetCommandRegexp(command string) (*regexp.Regexp, error) {
	return regexp.Compile("^!" + command)
}

func (d *Discord) ChangeBotNick(guildID, newNick string) (err error) {
	return d.session.GuildMemberNickname(guildID, "@me", newNick)
}

func (d *Discord) AddRoomListener(id, channelID, networkT, channelT string) error {
	if d.syncListeners == nil {
		d.syncListeners = make(map[string]func())
	}

	if d.syncs == nil {
		d.syncs = make(map[string][]*bots.Sync)
	}

	//Ignore if sync already exists
	if _, ok := d.syncListeners[id]; ok {
		return errors.New("[DISCORD] already existing sync")
	}

	//Ignore if sync already exists
	for _, v := range d.syncs {
		for _, target := range v {
			if target.ID == id {
				return errors.New("[SLACK] already existing sync")
			}
		}
	}

	sync := &bots.Sync{
		ID:      id,
		Network: networkT,
		Channel: channelT,
	}
	d.syncs[channelID] = append(d.syncs[channelID], sync)

	if d.session == nil {
		return errors.New("discord session nil")
	}

	listener := d.session.AddHandler(func(s *discordgo.Session, m *discordgo.MessageCreate) {
		// Ignore all messages created by the bot itself
		// This isn't required in this specific example but it's a good practice.
		if m.Author.ID == s.State.User.ID {
			return
		}

		if m.Author.Bot {
			hooks, err := GetHooksFromDB()
			if err != nil {
				logrus.Errorln("[DISCORD] Getting Hooks: ", err)
			}
			for _, v := range hooks {
				if m.Author.Username == v.Name {
					return
				}
			}
		}

		if channelID == m.ChannelID {
			ACommand := false
			for i := range d.commands {
				if i.MatchString(m.Content) {
					ACommand = true
				}
			}
			if !ACommand {
				metrics.DiscordMessages.Inc() // New Message

				channel, _ := s.State.Channel(m.ChannelID)
				member, _ := s.State.Member(channel.GuildID, m.Author.ID)
				var nick string
				if member != nil {
					nick = member.Nick
					if nick == "" {
						nick = m.Author.Username
					}
				} else {
					nick = m.Author.Username
				}

				// Send message
				//TODO more Media types!
				if len(m.Attachments) != 0 {
					for _, v := range m.Attachments {
						logrus.Debugln(v)
						resp, err := http.Get(v.URL)
						if err != nil {
							logrus.Errorln("[DISCORD] Failed getting the Image from Discord!")
							return
						}
						if strings.Contains(resp.Header.Get("Content-type"), "image/") {

							var locale *gotext.Locale
							data, err := util.DB.GetRoomData("discord", m.ChannelID)
							if err == sql.ErrNoRows {
								locale = gotext.NewLocale(util.LocalesRoot, "en")
							} else if err != nil {
								logrus.Errorln("[DISCORD] ", err)
							} else {
								locale = gotext.NewLocale(util.LocalesRoot, data.Language)
							}
							locale.AddDomain("sync")

							var text string
							if m.ContentWithMentionsReplaced() != "" {
								text = fmt.Sprintf("%s %s\n\n%s", nick, locale.GetD("sync", "sent an image"), m.ContentWithMentionsReplaced())
							} else if m.Content != "" {
								text = fmt.Sprintf("%s %s\n\n%s", nick, locale.GetD("sync", "sent an image"), m.Content)
							} else {
								text = fmt.Sprintf("%s %s", nick, locale.GetD("sync", "sent an image"))
							}

							buf := new(bytes.Buffer)
							avatar, err := d.session.UserAvatar(m.Author.ID)
							if err != nil {
								logrus.Errorln("[DISCORD] Failed getting the Avatar Image from Discord: ", err)
								//return
							} else {
								if err := jpeg.Encode(buf, avatar, nil); err != nil {
									logrus.Errorln("[DISCORD] Failed converting the Avatar Image from Discord: ", err)
									//return
								}
							}

							err = dispatching.SendImage(
								nick,
								buf.String(),
								m.Author.AvatarURL(""),
								text,
								networkT,
								channelT,
								v.Filename,
								v.URL,
								resp.Body,
							)
							if err != nil {
								logrus.Errorln("[DISCORD] Failed sending the Image to: ", networkT, " - ", channelT)
								return
							}
						}

						// Close the Body at the end to prevent any possible Memory Leak
						resp.Body.Close()
					}
				} else {
					buf := new(bytes.Buffer)
					avatar, err := d.session.UserAvatar(m.Author.ID)
					if err != nil {
						logrus.Errorln("[DISCORD] Failed getting the Avatar Image from Discord: ", err)
						//return
					} else {
						if err := jpeg.Encode(buf, avatar, nil); err != nil {
							logrus.Errorln("[DISCORD] Failed converting the Avatar Image from Discord: ", err)
							//return
						}
					}

					err = dispatching.SendText(
						nick,
						buf.String(),
						m.Author.AvatarURL(""),
						m.ContentWithMentionsReplaced(),
						networkT,
						channelT,
					)
					if err != nil {
						logrus.Errorln("[DISCORD] Failed to send text Message to: ", networkT, " - ", channelT)
						return
					}
				}
			}
		}
	})
	d.syncListeners[id] = listener
	return nil
}

func (d *Discord) RemoveRoomListener(id string) {
	sync := d.syncListeners[id]
	if sync != nil {
		sync()
	}
}

func convertToPNG(imageL io.Reader) (string, error) {
	img, _, err := image.Decode(imageL)
	if err != nil {
		return "", err
	}

	buf := new(bytes.Buffer)
	if err := jpeg.Encode(buf, img, nil); err != nil {
		return "", err
	}

	// This now takes a []byte of the buffer and base64 encodes it to a string
	// Never needing to create the image file all done in memory
	base64Img := base64.StdEncoding.EncodeToString(buf.Bytes())

	return "data:image/jpg;base64," + base64Img, nil
}

func (d *Discord) SendText(senderName, avatarS, avatarURL, channel, text string) error {
	if senderName == "" {
		_, err := d.session.ChannelMessageSend(channel, text)
		return err
	}
	hooks, err := GetHooksFromDB()
	if err != nil {
		logrus.Errorln("[DISCORD] Getting Hooks: ", err)
		return err
	}
	senderNameMax := senderName
	if len(senderNameMax) > 31 {
		senderNameMax = senderName[0:30]
	}

	hooksD, _ := d.session.ChannelWebhooks(channel)
	for _, v := range hooksD {
		//logrus.Infoln(v.ID)
		_, err := d.session.WebhookDeleteWithToken(v.ID, v.Token)
		if err != nil && err != discordgo.ErrJSONUnmarshal {
			logrus.Errorln("[DISCORD] WebhookDeleteWithToken 1: ", err, " ID: ", v.ID)
			if !strings.Contains(err.Error(), "Unknown Webhook") {
				return err
			}
		}
	}

	var hook *discordgo.Webhook
	for _, v := range hooks {
		hook = v
		_, err := d.session.WebhookDeleteWithToken(hook.ID, hook.Token)
		if err != nil && err != discordgo.ErrJSONUnmarshal {
			logrus.Errorln("[DISCORD] WebhookDeleteWithToken 2: ", err, " ID: ", hook.ID)
			if !strings.Contains(err.Error(), "Unknown Webhook") {
				return err
			}
		}

		err = RemoveHookFromDB(hook)
		if err != nil {
			logrus.Errorln("[DISCORD] RemoveHookFromDB: ", err)
			return err
		}
		hook = nil
	}

	// This should always be true!
	if hook == nil {
		logrus.Debugln("Creating new hook!")
		var err error
		hook, err = d.session.WebhookCreate(channel, senderNameMax, avatarS)
		if err != nil {
			logrus.Errorln("[DISCORD] Creating Hook: ", err)
			return err
		}

		err = SaveHookToDB(hook)
		if err != nil {
			logrus.Errorln("[DISCORD] Saving Hook: ", err)
			return err
		}
	}

	if avatarS != "" {
		logrus.Debugln("Setting hook avatar!")
		var avatarBuf = &bytes.Buffer{}
		avatarBuf.WriteString(avatarS)
		avatar, err := convertToPNG(avatarBuf)
		if err != nil {
			logrus.Errorln("[DISCORD] Converting image: ", err)
			// Dont exit as it might be a bug
			//return err
		}

		hook.Avatar = avatar
		_, err = d.session.WebhookEditWithToken(hook.ID, hook.Token, hook.Name, hook.Avatar)
		if err != nil {
			logrus.Errorln("[DISCORD] Updating Hook: ", err)
			return err
		}

		err = AddAvatarToHookDB(hook)
		if err != nil {
			logrus.Errorln("[DISCORD] Saving Hook: ", err)
			return err
		}
	}

	mentionsReplacedText := text
	submatches := mentionsIDRe.FindStringSubmatch(text)
	if len(submatches) > 0 {
		matchID := submatches[1]
		channelState, _ := d.session.State.Channel(channel)

		g, err := d.session.State.Guild(channelState.GuildID)
		if err == nil {
			for _, m := range g.Members {
				var nick string
				if m != nil {
					nick = m.Nick
					if nick == "" {
						nick = m.User.Username
					}
				} else {
					nick = m.User.Username
				}
				if strings.ToLower(nick) == strings.ToLower(matchID) {
					mentionsReplacedText = mentionsRe.ReplaceAllString(text, fmt.Sprintf("<@%s>", m.User.ID))
				}
			}
		}
	}

	msg := &discordgo.WebhookParams{
		Content: mentionsReplacedText,
	}

	_, err = d.session.WebhookExecute(hook.ID, hook.Token, true, msg)
	if err != nil {
		logrus.Errorln("[DISCORD] WebhookExecute: ", err)
		return err
	}

	_, err = d.session.WebhookDeleteWithToken(hook.ID, hook.Token)
	if err != nil && err != discordgo.ErrJSONUnmarshal {
		logrus.Errorln("[DISCORD] WebhookDeleteWithToken: ", err)
		return err
	}

	err = RemoveHookFromDB(hook)
	if err != nil {
		logrus.Errorln("[DISCORD] RemoveHookFromDB: ", err)
		return err
	}

	return nil
}

func (d *Discord) SendImage(senderName, avatarS, avatarURL, text, channel, filename, fileURL string, file io.Reader) error {
	// FIXME Webhook Images are broken
	msg := &discordgo.MessageSend{
		Content: text,
		File: &discordgo.File{
			Name:   filename,
			Reader: file,
		},
	}
	_, err := d.session.ChannelMessageSendComplex(channel, msg)

	return err
}

func (d *Discord) GetFormatter() formatter.Formatter {
	return &discordmd.DiscordMD{}
}
