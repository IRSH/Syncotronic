package discord

import (
	"bytes"
	"encoding/gob"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/util"
	"github.com/bwmarrin/discordgo"
)

func SaveHookToDB(hook *discordgo.Webhook) error {
	err := util.DB.AddTable(`CREATE TABLE IF NOT EXISTS discordHooks (id integer PRIMARY KEY, token text NOT NULL, channelID text NOT NULL, user text NOT NULL, name text NOT NULL, guildID text NOT NULL, avatar text NOT NULL)`)
	if err != nil {
		return err
	}

	db := util.DB.Open()

	tx, err := db.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare("INSERT INTO discordHooks (id, token, channelID, user, name, guildID, avatar) VALUES (?, ?, ?, ?, ?, ?, ?);")
	if err != nil {
		return err
	}
	defer stmt.Close()

	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	err = enc.Encode(hook.User)
	if err != nil {
		return err
	}

	_, err = stmt.Exec(hook.ID, hook.Token, hook.ChannelID, buf.String(), hook.Name, hook.GuildID, hook.Avatar)
	if err != nil {
		return err
	}

	return tx.Commit()
}

func AddAvatarToHookDB(hook *discordgo.Webhook) error {
	err := util.DB.AddTable(`CREATE TABLE IF NOT EXISTS discordHooks (id integer PRIMARY KEY, token text NOT NULL, channelID text NOT NULL, user text NOT NULL, name text NOT NULL, guildID text NOT NULL, avatar text NOT NULL)`)
	if err != nil {
		return err
	}

	db := util.DB.Open()

	tx, err := db.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare("UPDATE discordHooks SET avatar = ? WHERE id = ?;")
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(hook.Avatar, hook.ID)
	if err != nil {
		return err
	}

	return tx.Commit()
}

func GetHooksFromDB() (hooks []*discordgo.Webhook, err error) {
	err = util.DB.AddTable(`CREATE TABLE IF NOT EXISTS discordHooks (id integer PRIMARY KEY, token text NOT NULL, channelID text NOT NULL, user text NOT NULL, name text NOT NULL, guildID text NOT NULL, avatar text NOT NULL)`)
	if err != nil {
		return nil, err
	}

	db := util.DB.Open()

	tx, err := db.Begin()
	if err != nil {
		return nil, err
	}

	stmt, err := tx.Prepare("SELECT id, token, channelID, user, name, guildID, avatar FROM discordHooks;")
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var id string
		var token string
		var channelID string
		var user string
		var name string
		var guildID string
		var avatar string
		err = rows.Scan(&id, &token, &channelID, &user, &name, &guildID, &avatar)
		if err != nil {
			return nil, err
		}

		var buf bytes.Buffer
		buf.WriteString(user)
		dec := gob.NewDecoder(&buf)

		var userL = &discordgo.User{}

		err = dec.Decode(&userL)
		if err != nil {
			return nil, err
		}

		hook := &discordgo.Webhook{
			ID:        id,
			Token:     token,
			ChannelID: channelID,
			User:      userL,
			Name:      name,
			GuildID:   guildID,
			Avatar:    avatar,
		}

		hooks = append(hooks, hook)
	}

	err = tx.Commit()
	if err != nil {
		return nil, err
	}

	return hooks, err
}

func RemoveHookFromDB(hook *discordgo.Webhook) error {
	err := util.DB.AddTable(`CREATE TABLE IF NOT EXISTS discordHooks (id integer PRIMARY KEY, token text NOT NULL, channelID text NOT NULL, user text NOT NULL, name text NOT NULL, guildID text NOT NULL, avatar text NOT NULL)`)
	if err != nil {
		return err
	}

	db := util.DB.Open()

	tx, err := db.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare("DELETE FROM discordHooks WHERE id = ?;")
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(hook.ID)
	if err != nil {
		return err
	}

	return tx.Commit()
}
