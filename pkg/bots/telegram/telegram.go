// Package telegram provides a Telegram Bot
package telegram

import (
	"fmt"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/bots"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/commands"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/config"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/formatter"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/formatter/telegrammd"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/metrics"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/util"
	"github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
)

// Register Bot as Type Telegram
func init() {
	if util.Config == nil {
		util.Config = &config.Config{}
	}
	util.Config.RegisterBotType("telegram", &Telegram{})
}

// Define Telegram object and hold always by the bot used data in Memory
type Telegram struct {
	commands map[*regexp.Regexp]commands.Command
	syncs    map[string][]*bots.Sync
	bot      *tgbotapi.BotAPI
}

func (t *Telegram) GetSync() map[string][]*bots.Sync {
	return t.syncs
}

// StartBot gets called when starting the Framework and starts to listen to Telegram
func (t *Telegram) StartBot() error {
	var err error
	for i, v := range *util.Config.Bots {
		if strings.ToLower(i) == "telegram" {
			// FIXME not working as intended
			t.bot, err = tgbotapi.NewBotAPI(v.AccessToken)
			if err != nil {
				return err
			}
		}
	}

	if t.syncs == nil {
		t.syncs = make(map[string][]*bots.Sync)
	}

	if t.bot != nil {
		t.bot.Debug = *util.Debug

		u := tgbotapi.NewUpdate(0)
		u.Timeout = 10

		updates := t.bot.GetUpdatesChan(u)
	Updates:
		for update := range updates {
			if update.Message == nil {
				continue
			}

			if update.Message.From.ID == t.bot.Self.ID {
				continue
			}

			metrics.TelegramMessages.Inc() // New message

			for i, v := range t.commands {
				if i.MatchString(update.Message.Text) {
					err := v.Work("telegram", update.Message.From.UserName, "", strconv.FormatInt(update.Message.Chat.ID, 10), update.Message.Text, t.GetFormatter())
					if err != nil {
						logrus.Errorln(err)
					}
					continue Updates
				}
			}

			v, ok := t.syncs[strconv.FormatInt(update.Message.Chat.ID, 10)]
			if ok {
				for _, vt := range v {
					name := t.getNames(update.Message.From.FirstName, update.Message.From.LastName, update.Message.From.UserName)

					// Send Message
					//TODO Media Types
					if update.Message.Photo != nil && len(update.Message.Photo) != 0 {
						err := t.processPhotoMessage(update.Message.From.ID, update.Message.Chat.ID, name, update.Message.Text, vt.Network, vt.Channel, update.Message.Photo)
						if err != nil {
							logrus.Errorln(err.Error())
							continue
						}
					} else if update.Message.Sticker != nil {
						err := t.processStickerMessage(update.Message.From.ID, update.Message.Chat.ID, name, vt.Network, vt.Channel, update.Message.Sticker)
						if err != nil {
							logrus.Errorln(err.Error())
							continue
						}
					} else {
						err := t.processTextMessage(update.Message.From.ID, name, update.Message.Text, vt.Network, vt.Channel)
						if err != nil {
							logrus.Errorln(err.Error())
							continue
						}
					}
				}
			}
		}
	}

	return nil
}

//
func (t *Telegram) GetProfileData(userID int) (string, string, error) {
	profilePhotosConfig := tgbotapi.NewUserProfilePhotos(userID)
	profilePhotos, err := t.bot.GetUserProfilePhotos(profilePhotosConfig)
	if err != nil {
		return "", "", err
	}
	if profilePhotos.TotalCount == 0 {
		return "", "", nil
	}
	var profilePhoto tgbotapi.PhotoSize
	for _, e := range profilePhotos.Photos {
		for _, p := range e {
			if p.Height > profilePhoto.Height || p.Width > profilePhoto.Width {
				profilePhoto = p
			}
		}
	}

	profileImageURL, err := t.bot.GetFileDirectURL(profilePhoto.FileID)
	if err != nil {
		return "", "", errors.Wrap(err, "Failed getting the Profile Image Direct URL from Telegram!")
	}

	parsedProfileImageURL, err := url.Parse(profileImageURL)
	if err != nil {
		return "", "", errors.Wrap(err, "Failed parsing the Profile Image Direct URL!")
	}

	resp, err := http.Get(parsedProfileImageURL.String())
	if err != nil {
		return "", "", errors.Wrap(err, "Failed getting the Profile Image from Telegram!")
	}

	defer resp.Body.Close()

	var avatarS string
	if resp.StatusCode == http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return "", "", err
		}
		avatarS = string(bodyBytes)
	}

	return avatarS, parsedProfileImageURL.String(), nil
}

//
func (t *Telegram) StopBot() error {
	logrus.Infoln("Stopping Telegram...")
	t.bot.StopReceivingUpdates()
	util.Botwg.Done()
	return nil
}

//
func (t *Telegram) AddCommand(command commands.Command, regexpL *regexp.Regexp) error {
	if t.commands == nil {
		t.commands = make(map[*regexp.Regexp]commands.Command)
	}
	t.commands[regexpL] = command
	return nil
}

//
func (t *Telegram) GetCommandRegexp(command string) (*regexp.Regexp, error) {
	return regexp.Compile("/" + command)
}

//
func (t *Telegram) SendText(senderName, avatarS, avatarURL, channel, text string) error {
	channeli, err := strconv.ParseInt(channel, 10, 64)
	if err != nil {
		return err
	}

	var msgText string

	if senderName != "" {
		msgText = fmt.Sprintf("%s: %s", t.GetFormatter().GetBold(senderName), text)
	} else {
		msgText = text
	}

	msg := tgbotapi.MessageConfig{
		BaseChat: tgbotapi.BaseChat{
			ChatID:           channeli,
			ReplyToMessageID: 0,
		},
		Text:                  msgText,
		ParseMode:             "Markdown",
		DisableWebPagePreview: false,
	}

	_, err = t.bot.Send(msg)
	return err
}

//
func (t *Telegram) SendImage(senderName, avatarS, avatarURL, text, channel, filename, fileURL string, file io.Reader) error {
	channeli, err := strconv.ParseInt(channel, 10, 64)
	if err != nil {
		return err
	}

	reader := tgbotapi.FileReader{Name: filename, Reader: file, Size: -1}

	msg := tgbotapi.NewPhotoUpload(channeli, reader)
	msg.Caption = text

	_, err = t.bot.Send(msg)
	return err
}

//
func (t *Telegram) AddRoomListener(id, channelID, networkT, channelT string) error {
	if t.syncs == nil {
		t.syncs = make(map[string][]*bots.Sync)
	}

	//Ignore if sync already exists
	for _, v := range t.syncs {
		for _, target := range v {
			if target.ID == id {
				return errors.New("[TELEGRAM] already existing sync")
			}
		}
	}

	sync := &bots.Sync{
		ID:      id,
		Network: networkT,
		Channel: channelT,
	}
	t.syncs[channelID] = append(t.syncs[channelID], sync)
	return nil
}

//
func (t *Telegram) RemoveRoomListener(id string) {
	for i, v := range t.syncs {
		for targetI, target := range v {
			if target.ID == id {
				v[targetI] = v[len(v)-1]
				v[len(v)-1] = nil
				t.syncs[i] = v[:len(v)-1]
			}
		}
	}
}

//
func (t *Telegram) GetFormatter() formatter.Formatter {
	return &telegrammd.TelegramMD{}
}
