package telegram

import (
	"database/sql"
	"fmt"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/distributor/dispatching"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/util"
	"github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/leonelquinteros/gotext"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

//
func (t *Telegram) getNames(first, last, username string) string {
	var name string
	if first != "" && last != "" {
		name = fmt.Sprintf("%s %s", first, last)
	} else if first != "" {
		name = first
	} else {
		name = username
	}
	return name
}

//
func (t *Telegram) processPhotoMessage(userId int, chatId int64, name, pureText, network, channel string, photoArray []tgbotapi.PhotoSize) error {
	var photo tgbotapi.PhotoSize
	for _, e := range photoArray {
		if e.Height > photo.Height || e.Width > photo.Width {
			photo = e
		}
	}
	imageURL, err := t.bot.GetFileDirectURL(photo.FileID)
	if err != nil {
		return errors.New("[TELEGRAM] Failed getting the Image Direct URL from Telegram!")
	}

	parsedImageURL, err := url.Parse(imageURL)
	if err != nil {
		return errors.New("[TELEGRAM] Failed parsing the Image Direct URL!")
	}

	resp, err := http.Get(parsedImageURL.String())
	if err != nil {
		return errors.New("[TELEGRAM] Failed getting the Image from Telegram!")
	}

	var locale *gotext.Locale
	data, err := util.DB.GetRoomData("telegram", strconv.FormatInt(chatId, 10))
	if err == sql.ErrNoRows {
		locale = gotext.NewLocale(util.LocalesRoot, "en")
	} else if err != nil {
		logrus.Errorln("[TELEGRAM] ", err)
	} else {
		locale = gotext.NewLocale(util.LocalesRoot, data.Language)
	}
	locale.AddDomain("sync")

	var text string
	if pureText != "" {
		text = fmt.Sprintf("%s %s\n\n%s", name, locale.GetD("sync", "sent an image"), pureText)
	} else {
		text = fmt.Sprintf("%s %s", name, locale.GetD("sync", "sent an image"))
	}

	pathSplit := strings.Split(parsedImageURL.EscapedPath(), "/")
	filename := photo.FileID + pathSplit[len(pathSplit)-1]

	avatarS, avatarURL, err := t.GetProfileData(userId)
	if err != nil {
		logrus.Errorln("[TELEGRAM] ", err)
	}

	err = dispatching.SendImage(
		name,
		avatarS,
		avatarURL,
		text,
		network,
		channel,
		filename,
		parsedImageURL.String(),
		resp.Body,
	)
	if err != nil {
		return errors.New(fmt.Sprintf("[TELEGRAM] Failed to send Image message to: %s-%s", network, channel))
	}

	// Close the Body at the end to prevent any possible Memory Leak
	return resp.Body.Close()
}

//
func (t *Telegram) processStickerMessage(userId int, chatId int64, name, network, channel string, sticker *tgbotapi.Sticker) error {
	stickerURL, err := t.bot.GetFileDirectURL(sticker.FileID)
	if err != nil {
		return errors.New("[TELEGRAM] Failed getting the Sticker Direct URL from Telegram!")
	}

	parsedStickerURL, err := url.Parse(stickerURL)
	if err != nil {
		return errors.New("[TELEGRAM] Failed parsing the Sticker Direct URL!")
	}

	resp, err := http.Get(parsedStickerURL.String())
	if err != nil {
		return errors.New("[TELEGRAM] Failed getting the Sticker from Telegram!")
	}

	var locale *gotext.Locale
	data, err := util.DB.GetRoomData("telegram", strconv.FormatInt(chatId, 10))
	if err == sql.ErrNoRows {
		locale = gotext.NewLocale(util.LocalesRoot, "en")
	} else if err != nil {
		logrus.Errorln("[TELEGRAM] ", err)
	} else {
		locale = gotext.NewLocale(util.LocalesRoot, data.Language)
	}
	locale.AddDomain("sync")

	var text = fmt.Sprintf("%s %s", name, locale.GetD("sync", "sent a sticker"))

	pathSplit := strings.Split(parsedStickerURL.EscapedPath(), "/")
	filename := sticker.FileID + pathSplit[len(pathSplit)-1]

	avatarS, avatarURL, err := t.GetProfileData(userId)
	if err != nil {
		logrus.Errorln("[TELEGRAM] ", err)
	}

	png, err := webmToPNG(resp.Body)
	if err != nil {
		logrus.Errorln("[TELEGRAM] ", err)
	}

	err = dispatching.SendImage(
		name,
		avatarS,
		avatarURL,
		text,
		network,
		channel,
		filename,
		parsedStickerURL.String(),
		png,
	)
	if err != nil {
		return errors.New(fmt.Sprintf("[TELEGRAM] Failed to send Sticker message to: %s-%s", network, channel))
	}

	// Close the Body at the end to prevent any possible Memory Leak
	return resp.Body.Close()
}

//
func (t *Telegram) processTextMessage(id int, name, text, network, channel string) error {
	avatarS, avatarURL, err := t.GetProfileData(id)
	if err != nil {
		logrus.Errorln("[TELEGRAM] ", err)
	}
	err = dispatching.SendText(
		name,
		avatarS,
		avatarURL,
		text,
		network,
		channel,
	)
	if err != nil {
		return errors.New(fmt.Sprintf("[TELEGRAM] Failed to send text message to: %s-%s", network, channel))
	}
	return nil
}
