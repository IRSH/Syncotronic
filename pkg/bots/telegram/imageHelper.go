package telegram

import (
	"bytes"
	"github.com/chai2010/webp"
	"image/png"
	"io"
	"io/ioutil"
)

func webmToPNG(data io.Reader) (io.Reader, error) {
	dataB, err := ioutil.ReadAll(data)
	m, err := webp.DecodeRGBA(dataB)
	if err != nil {
		return nil, err
	}

	var b bytes.Buffer
	err = png.Encode(&b, m)

	if err != nil {
		return nil, err
	}
	r := bytes.NewReader(b.Bytes())
	return r, nil
}
