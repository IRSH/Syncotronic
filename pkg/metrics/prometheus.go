package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
)

// Init Types
var SlackLatency = promauto.NewGauge(prometheus.GaugeOpts{
	Name: "slack_latency",
	Help: "The latency Slack does report in milliseconds.",
})

var SlackMessages = promauto.NewCounter(prometheus.CounterOpts{
	Name: "slack_messages",
	Help: "The number of messages sent from slack",
})

var TelegramMessages = promauto.NewCounter(prometheus.CounterOpts{
	Name: "telegram_messages",
	Help: "The number of messages sent from Telegram",
})

var DiscordMessages = promauto.NewCounter(prometheus.CounterOpts{
	Name: "discord_messages",
	Help: "The number of messages sent from Discord",
})

// Init Webserver
func Start() {
	http.Handle("/metrics", promhttp.Handler())
	http.ListenAndServe(":9999", nil)
}
