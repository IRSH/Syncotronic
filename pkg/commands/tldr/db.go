package tldr

import (
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/util"
)

func GetTLDRForRoom() error {
	err := util.DB.AddTable(`CREATE TABLE IF NOT EXISTS tldr (id integer, service text NOT NULL, channel text NOT NULL, tldr_text text NOT NULL)`)
	if err != nil {
		return err
	}
	return nil
}
