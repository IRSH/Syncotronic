package tldr

import (
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/config"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/distributor/dispatching"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/formatter"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/util"
	"github.com/mattn/go-shellwords"
)

func init() {
	if util.Config == nil {
		util.Config = &config.Config{}
	}
	util.Config.RegisterCommandType("tldr", &TLDR{})
}

// Help holds any command information
type TLDR struct {
}

// Work does prepare the help response
func (t *TLDR) Work(sourcebot, user, guild, channel, message string, formatter formatter.Formatter) error {
	args, err := shellwords.Parse(message)
	if err != nil {
		return dispatching.SendText("", "", "", "[ERROR] Failed to parse arguments", sourcebot, channel)
	}
	if len(args) == 0 {
		err = t.display(sourcebot, user, channel, formatter)
	} else {
		err = t.add(sourcebot, user, channel, message, formatter)
	}
	return err
}

func (t *TLDR) display(sourcebot, user, channel string, formatter formatter.Formatter) error {
	return nil
}

func (t *TLDR) add(sourcebot, user, channel, message string, formatter formatter.Formatter) error {
	return nil
}

// AddHelp adds help information about the command for being listed under the help output
func (t *TLDR) AddHelp() {
	if util.Helps == nil {
		util.Helps = make(map[string]string)
	}

	util.Helps["tldr"] = "Show tldr list or add items to it"
}
