package nick

import (
	"fmt"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/bots/discord"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/config"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/distributor/dispatching"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/formatter"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/util"
	"github.com/mattn/go-shellwords"
)

// init is used to register the Command
func init() {
	if util.Config == nil {
		util.Config = &config.Config{}
	}

	util.Config.RegisterCommandType("nick", &Nick{})
}

// Nick holds any command information
type Nick struct{}

func (n *Nick) Work(sourcebot, user, guild, channel, message string, formatter formatter.Formatter) error {
	if sourcebot != "discord" {
		return dispatching.SendText("", "", "", "This platform is not supported when changing Nicks", sourcebot, channel)
	}

	args, err := shellwords.Parse(message)
	if err != nil {
		return dispatching.SendText("", "", "", "[ERROR] Failed to parse arguments", sourcebot, channel)
	}
	if len(args) > 2 {
		return dispatching.SendText("", "", "", fmt.Sprintf("Too many arguments. Please only have %s!", "<newNickname>"), sourcebot, channel)
	} else if len(args) < 2 {
		return dispatching.SendText("", "", "", fmt.Sprintf("Not enough arguments. Please have %s!", "<newNickname>"), sourcebot, channel)
	}

	switch d := util.Config.BotsMap["discord"].(type) {
	case *discord.Discord:
		err := d.ChangeBotNick(guild, args[1])
		if err != nil {
			return dispatching.SendText("", "", "", "Failed to change Nickname of Bot", sourcebot, channel)
		}
	}
	return dispatching.SendText("", "", "", "Nickname of Bot changed", sourcebot, channel)
}

func (n *Nick) AddHelp() {
	if util.Helps == nil {
		util.Helps = make(map[string]string)
	}

	util.Helps["nick"] = "Allows to change bot name in discord"
}
