// help is a simple help command
package help

import (
	"bytes"
	"database/sql"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/config"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/distributor/dispatching"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/formatter"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/util"
	"github.com/leonelquinteros/gotext"
	"github.com/sirupsen/logrus"
	"regexp"
	"strings"
)

func init() {
	if util.Config == nil {
		util.Config = &config.Config{}
	}
	util.Config.RegisterCommandType("help", &Help{})
}

// Help holds any command information
type Help struct {
}

// Work does prepare the help response
func (h *Help) Work(sourcebot, user, guild, channel, message string, formatter formatter.Formatter) error {
	err := util.DB.AddTable(`CREATE TABLE IF NOT EXISTS rooms (id integer PRIMARY KEY, service text NOT NULL, channel text NOT NULL, language text NOT NULL)`)
	if err != nil {
		logrus.Errorln(err)
	}

	var textB bytes.Buffer
	textB.WriteString(formatter.GetTitle(gotext.GetD("help", "Help")))
	textB.WriteString("\n")
	var list []string
	for i, v := range util.Helps {
		var listB bytes.Buffer
		var argregexp = regexp.MustCompile(`<.+?>`)

		helptext := i
		for _, match := range argregexp.FindAllString(helptext, -1) {
			newMatch := formatter.GetCodeSimple(match)
			helptext = strings.Replace(helptext, match, newMatch, -1)
		}

		listB.WriteString(helptext)
		listB.WriteString(" - ")

		var locale *gotext.Locale
		data, err := util.DB.GetRoomData(sourcebot, channel)
		if err == sql.ErrNoRows {
			locale = gotext.NewLocale(util.LocalesRoot, "en")
		} else if err != nil {
			sentErr := dispatching.SendText("", "", "", "Cant sent Help... Some error happened", sourcebot, channel)
			if sentErr != nil {
				return sentErr
			}
			return err
		} else {
			locale = gotext.NewLocale(util.LocalesRoot, data.Language)
		}
		locale.AddDomain(i)

		commandtext := locale.GetD(i, v)
		for _, match := range argregexp.FindAllString(commandtext, -1) {
			newMatch := formatter.GetCodeSimple(match)
			commandtext = strings.Replace(commandtext, match, newMatch, -1)
		}

		listB.WriteString(commandtext)
		list = append(list, listB.String())
	}
	textB.WriteString(formatter.GetList(list))
	logrus.Debugln("Response: ", textB.String())
	return dispatching.SendText("", "", "", textB.String(), sourcebot, channel)
}

// AddHelp adds help information about the command for being listed under the help output
func (h *Help) AddHelp() {
	if util.Helps == nil {
		util.Helps = make(map[string]string)
	}

	util.Helps["help"] = "Show this help message"
}
