package whereami

import (
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/config"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/distributor/dispatching"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/formatter"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/util"
)

// init is used to register the Command
func init() {
	if util.Config == nil {
		util.Config = &config.Config{}
	}

	util.Config.RegisterCommandType("whereami", &WhereAmI{})
}

// WhereAmI holds any command information
type WhereAmI struct {
}

// Work does prepare the whereami response
func (w *WhereAmI) Work(sourcebot, user, guild, channel, message string, formatter formatter.Formatter) error {
	return dispatching.SendText("", "", "", "Current Platform: "+formatter.GetCode(sourcebot)+"\nCurrent ChannelID: "+formatter.GetCode(channel), sourcebot, channel)
}

// AddHelp adds help information about the command for being listed under the help output
func (w *WhereAmI) AddHelp() {
	if util.Helps == nil {
		util.Helps = make(map[string]string)
	}

	util.Helps["whereami"] = "Returns the platform type and channelID of the current room"
}
