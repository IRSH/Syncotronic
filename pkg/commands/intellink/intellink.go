package intellink

import (
	"fmt"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/config"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/distributor/dispatching"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/formatter"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/util"
	"net/url"
	"regexp"
	"strings"
)

var regex = regexp.MustCompile(`(?m)https?://(www\.|intel\.)ingress.com/intel\?ll=([^ ]|$)*`)

// init is used to register the Command
func init() {
	if util.Config == nil {
		util.Config = &config.Config{}
	}

	util.Config.RegisterRawCommandType("intellink", &IntelLink{}, regex)
}

// IntelLink holds any command information
type IntelLink struct{}

func (i *IntelLink) Work(sourcebot, user, guild, channel, message string, formatter formatter.Formatter) error {
	err := i.Send(message, sourcebot, channel, true)
	if err != nil {
		return err
	}

	match := regex.FindAllStringSubmatch(message, -1)

	parsedURL, err := url.Parse(match[0][0])
	if err != nil {
		return dispatching.SendText("", "", "", "[ERROR] Failed to parse url", sourcebot, channel)
	}

	queries := parsedURL.Query()

	pll := queries.Get("pll")
	if pll != "" {
		latlong := strings.Replace(pll, ",", "+", -1)

		gurl := fmt.Sprintf("http://maps.google.com/maps?q=loc:%s", latlong)
		wurl := fmt.Sprintf("https://www.waze.com/ul?ll=%s&navigate=yes&zoom=17", pll)

		return i.Send(fmt.Sprintf("Links ... \nGoogle Maps: %s\nWaze Maps: %s", gurl, wurl), sourcebot, channel, false)
	}

	// return dispatching.SendText("", "", "", "Intel Link erkannt", sourcebot, channel)
	return nil
}

func (i *IntelLink) Send(message, sourcebot, channel string, onlyOthers bool) error {
	syncs := util.Config.BotsMap[sourcebot].GetSync()
	syncsHere := syncs[channel]
	var err error
	for _, sync := range syncsHere {
		err = dispatching.SendText("", "", "", message, sync.Network, sync.Channel)
	}
	if err != nil {
		return err
	}

	if !onlyOthers {
		return dispatching.SendText("", "", "", message, sourcebot, channel)
	}

	return nil
}

func (i *IntelLink) AddHelp() {
	if util.Helps == nil {
		util.Helps = make(map[string]string)
	}

	util.Helps["intellink"] = "Listens to intel links and returns information about them"
}
