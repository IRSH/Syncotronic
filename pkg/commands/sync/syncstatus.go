package sync

import (
	"fmt"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/db"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/distributor/dispatching"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/formatter"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/util"
)

// Syncstatus holds any command information
type Syncstatus struct {
}

// Work does prepare the syncstatus response
func (s *Syncstatus) Work(sourcebot, user, guild, channel, message string, formatter formatter.Formatter) error {
	syncs, err := util.DB.GetSyncPools()
	if err != nil {
		return dispatching.SendText("", "", "", "Failed to access Database", sourcebot, channel)
	}
	var currentRoomSync []*db.Sync
	for _, syncList := range syncs {
		for _, sync := range syncList {
			if sync.Network == sourcebot && sync.ChannelID == channel {
				currentRoomSync = syncList
			}
		}
	}

	var syncList []string
	for _, v := range currentRoomSync {
		syncList = append(syncList, fmt.Sprintf("%s/%s", v.Network, v.ChannelID))
	}

	text := fmt.Sprintf("%s\n\n%s", formatter.GetTitle("Targets:"), formatter.GetList(syncList))
	return dispatching.SendText("", "", "", text, sourcebot, channel)
}

// AddHelp adds help information about the command for being listed under the help output
func (s *Syncstatus) AddHelp() {
	if util.Helps == nil {
		util.Helps = make(map[string]string)
	}

	util.Helps["syncstatus"] = "Shows the different sync targets"
}
