package sync

import (
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/config"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/util"
)

// init is used to register the Command
func init() {
	if util.Config == nil {
		util.Config = &config.Config{}
	}

	util.Config.RegisterCommandType("startsync", &Startsync{})
	util.Config.RegisterCommandType("stopsync", &Stopsync{})
	util.Config.RegisterCommandType("syncstatus", &Syncstatus{})
}
