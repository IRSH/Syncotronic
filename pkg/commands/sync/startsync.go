package sync

import (
	"fmt"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/distributor/dispatching"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/distributor/sync"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/formatter"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/util"
	"github.com/mattn/go-shellwords"
)

// Startsync holds any command information
type Startsync struct {
}

// Work does prepare the startsync response
func (s *Startsync) Work(sourcebot, user, guild, channel, message string, formatter formatter.Formatter) error {
	args, err := shellwords.Parse(message)
	if err != nil {
		return dispatching.SendText("", "", "", "[ERROR] Failed to parse arguments", sourcebot, channel)
	}
	if len(args) > 3 {
		return dispatching.SendText("", "", "", fmt.Sprintf("Too many arguments. Please only have %s and %s in the correct order!", formatter.GetCodeSimple("<destinationPlatform>"), formatter.GetCodeSimple("<destinationChannel>")), sourcebot, channel)
	} else if len(args) < 3 {
		return dispatching.SendText("", "", "", fmt.Sprintf("Not enough arguments. Please have %s and %s as arguments in the correct order!", formatter.GetCodeSimple("<destinationPlatform>"), formatter.GetCodeSimple("<destinationChannel>")), sourcebot, channel)
	}
	return sync.RegisterSync(sourcebot, args[1], channel, args[2])
}

// AddHelp adds help information about the command for being listed under the help output
func (s *Startsync) AddHelp() {
	if util.Helps == nil {
		util.Helps = make(map[string]string)
	}

	util.Helps["startsync <destinationPlatform> <destinationChannel>"] = "Starts a Sync between this Room and the Destination Room"
}
