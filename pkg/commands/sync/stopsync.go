package sync

import (
	"fmt"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/distributor/dispatching"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/distributor/sync"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/formatter"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/util"
	"github.com/mattn/go-shellwords"
)

// Stopsync holds any command information
type Stopsync struct{}

// Work does prepare the startsync response
func (s *Stopsync) Work(sourcebot, user, guild, channel, message string, formatter formatter.Formatter) error {
	args, err := shellwords.Parse(message)
	if err != nil {
		return dispatching.SendText("", "", "", "[ERROR] Failed to parse arguments", sourcebot, channel)
	}
	if len(args) > 3 {
		return dispatching.SendText("", "", "", fmt.Sprintf("Too many arguments. Please only have %s and %s in the correct order!", formatter.GetCodeSimple("<destinationPlatform>"), formatter.GetCodeSimple("<destinationChannel>")), sourcebot, channel)
	} else if len(args) < 3 {
		return dispatching.SendText("", "", "", fmt.Sprintf("Not enough arguments. Please have %s and %s as arguments in the correct order!", formatter.GetCodeSimple("<destinationPlatform>"), formatter.GetCodeSimple("<destinationChannel>")), sourcebot, channel)
	}
	return sync.RemoveSync(channel, sourcebot, args[2], args[1])
}

// AddHelp adds help information about the command for being listed under the help output
func (s *Stopsync) AddHelp() {
	if util.Helps == nil {
		util.Helps = make(map[string]string)
	}

	util.Helps["stopsync <destinationPlatform> <destinationChannel>"] = "Removes a Sync between this Room and the Destination Room"
}
