package setlanguage

import (
	"fmt"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/config"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/db"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/distributor/dispatching"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/formatter"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/util"
	"github.com/mattn/go-shellwords"
	"github.com/sirupsen/logrus"
)

func init() {
	if util.Config == nil {
		util.Config = &config.Config{}
	}

	util.Config.RegisterCommandType("setlanguage", &SetLanguage{})
}

type SetLanguage struct{}

func (s *SetLanguage) Work(sourcebot, user, guild, channel, message string, formatter formatter.Formatter) error {
	err := util.DB.AddTable(`CREATE TABLE IF NOT EXISTS rooms (id integer PRIMARY KEY, service text NOT NULL, channel text NOT NULL, language text NOT NULL)`)
	if err != nil {
		logrus.Errorln(err)
	}

	args, err := shellwords.Parse(message)
	if err != nil {
		return dispatching.SendText("", "", "", "[ERROR] Failed to parse arguments", sourcebot, channel)
	}
	if len(args) > 2 {
		return dispatching.SendText("", "", "", fmt.Sprintf("Too many arguments. Please only have %s!", formatter.GetCodeSimple("<languageCode>")), sourcebot, channel)
	} else if len(args) < 2 {
		return dispatching.SendText("", "", "", fmt.Sprintf("Not enough arguments. Please have %s!", formatter.GetCodeSimple("<languageCode>")), sourcebot, channel)
	}

	roomData := &db.RoomData{
		Language: args[1],
	}

	err = util.DB.SetRoomData(sourcebot, channel, roomData)
	if err != nil {
		logrus.Errorln(err)
		return dispatching.SendText("", "", "", fmt.Sprintf("Failed to set language"), sourcebot, channel)
	}

	return dispatching.SendText("", "", "", fmt.Sprintf("Language set to: %s", args[1]), sourcebot, channel)
}

// AddHelp adds help information about the command for being listed under the help output
func (s *SetLanguage) AddHelp() {
	if util.Helps == nil {
		util.Helps = make(map[string]string)
	}

	util.Helps["setlanguage"] = "Set the channel language using ISO 639-1 codes (for example de\\_DE)"
}
