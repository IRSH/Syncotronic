package commands

import "git.nordgedanken.de/IRSH/Syncotronic/pkg/formatter"

type Command interface {
	Work(sourcebot, user, guild, channel, message string, formatter formatter.Formatter) error
	AddHelp()
}
