package sqlite

import "git.nordgedanken.de/IRSH/Syncotronic/pkg/db"

func (s *SQLite) GetRoomData(service, channel string) (*db.RoomData, error) {
	if s.db == nil {
		s.db = s.Open()
	}

	stmt, err := s.db.Prepare(`SELECT id, language FROM rooms WHERE service=$1 AND channel=$2`)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(service, channel)

	var id string
	var language string
	err = row.Scan(&id, &language)
	if err != nil {
		return nil, err
	}

	roomData := &db.RoomData{
		DBId:     id,
		Language: language,
	}

	return roomData, nil
}

func (s *SQLite) SetRoomData(service, channel string, data *db.RoomData) error {
	if s.db == nil {
		s.db = s.Open()
	}

	tx, err := s.db.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare("INSERT INTO rooms (service, channel, language) VALUES (?, ?, ?)")
	if err != nil {
		return err
	}
	defer stmt.Close()

	result, err := stmt.Exec(service, channel, data.Language)
	if err != nil {
		return err
	}

	_, err = result.LastInsertId()
	if err != nil {
		return err
	}

	return tx.Commit()
}
