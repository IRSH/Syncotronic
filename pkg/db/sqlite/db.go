package sqlite

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3" // Go-sqlite3 side effect import needed to use SQLite3 databases
	"github.com/pkg/errors"
	"github.com/shibukawa/configdir"
	log "github.com/sirupsen/logrus"
	"path/filepath"
)

// Init prepares the DB by opening it and creating the required tables if needed
func (s *SQLite) Init() (err error) {
	log.Infoln("Start setting up DB")
	var openErr error

	// Open the data.db file. It will be created if it doesn't exist.
	configDirs := configdir.New("IRSH", "Syncotronic")
	filePath := filepath.ToSlash(configDirs.QueryFolders(configdir.Global)[0].Path)

	log.Debugln("DBFilePath: ", filePath+"/data.db")
	s.db, openErr = sql.Open("sqlite3", filePath+"/data.db")
	if openErr != nil {
		err = errors.Wrap(openErr, "Init Open")
		return
	}

	log.Infoln("Finished DB Setup")
	return
}

// Open returns the in Init() created db variable
func (s *SQLite) Open() *sql.DB {
	if s.db != nil {
		return s.db
	}
	return nil
}

func (s *SQLite) AddTable(tableSql string) error {
	if s.db == nil {
		s.db = s.Open()
	}

	_, execErr := s.db.Exec(tableSql)
	return errors.Wrap(execErr, "Init Exec")
}
