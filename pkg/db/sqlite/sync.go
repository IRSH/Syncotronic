package sqlite

import (
	"bytes"
	"encoding/gob"
	"git.nordgedanken.de/IRSH/Syncotronic/pkg/db"
	"strconv"
)

// AddSyncPool inits a new Sync Pool
// syncs map consist of room as key and network as value
// id is used if syncpool was found
func (s *SQLite) AddSyncPool(syncs []*db.Sync, id string) (int64, error) {
	if s.db == nil {
		s.db = s.Open()
	}

	tx, err := s.db.Begin()
	if err != nil {
		return -1, err
	}

	if id == "" {
		stmt, err := tx.Prepare("INSERT INTO sync (targets) VALUES (?)")
		if err != nil {
			return -1, err
		}
		defer stmt.Close()

		var buf bytes.Buffer
		enc := gob.NewEncoder(&buf)
		err = enc.Encode(syncs)
		if err != nil {
			return -1, err
		}

		result, err := stmt.Exec(buf.String())
		if err != nil {
			return -1, err
		}

		newid, err := result.LastInsertId()
		if err != nil {
			return -1, err
		}

		return newid, tx.Commit()
	} else {
		// Add Sync to syncPool
		var buf bytes.Buffer
		enc := gob.NewEncoder(&buf)
		err = enc.Encode(syncs)
		if err != nil {
			return -1, err
		}

		stmt, err := tx.Prepare("UPDATE sync SET targets = ? WHERE id = ?;")
		if err != nil {
			return -1, err
		}
		defer stmt.Close()

		result, err := stmt.Exec(buf.String(), id)
		if err != nil {
			return -1, err
		}

		newid, err := result.LastInsertId()
		if err != nil {
			return -1, err
		}

		return newid, tx.Commit()
	}

}

func (s *SQLite) GetSyncPool(id int64) ([]*db.Sync, error) {
	if s.db == nil {
		s.db = s.Open()
	}

	stmt, err := s.db.Prepare(`SELECT targets FROM sync WHERE id=$1`)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	idS := strconv.FormatInt(id, 10)
	row := stmt.QueryRow(idS)

	var targets string
	err = row.Scan(&targets)
	if err != nil {
		return nil, err
	}

	var buf bytes.Buffer
	buf.WriteString(targets)
	dec := gob.NewDecoder(&buf)

	var syncs []*db.Sync
	err = dec.Decode(&syncs)
	if err != nil {
		return nil, err
	}

	return syncs, nil
}

func (s *SQLite) GetSyncPools() ([][]*db.Sync, error) {
	if s.db == nil {
		s.db = s.Open()
	}

	stmt, err := s.db.Prepare(`SELECT targets FROM sync`)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var syncsList [][]*db.Sync

	for rows.Next() {
		var targets string
		err = rows.Scan(&targets)
		if err != nil {
			return nil, err
		}

		var buf bytes.Buffer
		buf.WriteString(targets)
		dec := gob.NewDecoder(&buf)

		var syncsL []*db.Sync
		err = dec.Decode(&syncsL)
		if err != nil {
			return nil, err
		}
		syncsList = append(syncsList, syncsL)
	}

	return syncsList, nil
}

func (s *SQLite) GetSyncPoolByChannel(destinationC, destinationS, originC, originS string) (string, []*db.Sync, error) {
	if s.db == nil {
		s.db = s.Open()
	}

	stmt, err := s.db.Prepare(`SELECT id, targets FROM sync`)
	if err != nil {
		return "", nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		return "", nil, err
	}
	defer rows.Close()

	var syncsList = make(map[string][]*db.Sync)
	var syncs []*db.Sync
	var id string

	for rows.Next() {
		var targets string
		err = rows.Scan(&id, &targets)
		if err != nil {
			return "", nil, err
		}

		var buf bytes.Buffer
		buf.WriteString(targets)
		dec := gob.NewDecoder(&buf)

		var syncsL []*db.Sync
		err = dec.Decode(&syncsL)
		if err != nil {
			return "", nil, err
		}
		syncsList[id] = syncsL
	}

	for i, v := range syncsList {
		var one bool
		var two bool
		for _, v := range v {
			if v.Network == originS && v.ChannelID == originC {
				one = true
			}
			if v.Network == destinationS && v.ChannelID == destinationC {
				two = true
			}
		}
		if one || two {
			syncs = v
			id = i
			break
		} else {
			id = ""
		}
	}
	return id, syncs, rows.Err()
}

func (s *SQLite) RemoveSyncByID(id string, syncs []*db.Sync, destinationS, destinationC string) error {
	if s.db == nil {
		s.db = s.Open()
	}

	tx, err := s.db.Begin()
	if err != nil {
		return err
	}

	// Remove Sync from syncPool
	for i := 0; i < len(syncs); i++ {
		v := syncs[i]
		if v.Network == destinationS && v.ChannelID == destinationC {
			syncs = append(syncs[:i], syncs[i+1:]...)
		}
	}
	stmt, err := tx.Prepare("UPDATE sync SET targets = ? WHERE id = ?;")
	if err != nil {
		return err
	}
	defer stmt.Close()

	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	err = enc.Encode(syncs)
	if err != nil {
		return err
	}

	_, err = stmt.Exec(buf.String(), id)
	if err != nil {
		return err
	}

	return tx.Commit()
}
