package db

import (
	"database/sql"
)

type RoomData struct {
	DBId     string
	Language string
}

type Sync struct {
	Network   string
	ChannelID string
	UUID      string
}

// DB defines a Interface to allow multiple DB Implementations
type DB interface {
	Init() error
	Open() *sql.DB

	AddTable(tableSql string) error

	AddSyncPool(syncs []*Sync, id string) (int64, error)
	GetSyncPool(id int64) ([]*Sync, error)
	GetSyncPools() ([][]*Sync, error)
	GetSyncPoolByChannel(destinationC, destinationS, originC, originS string) (string, []*Sync, error)
	RemoveSyncByID(id string, syncs []*Sync, destinationS, destinationC string) error

	GetRoomData(service, channel string) (*RoomData, error)
	SetRoomData(service, channel string, data *RoomData) error
}
